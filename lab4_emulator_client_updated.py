# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np


#TODO 1: modify the following parameters
#Starting and end index, modify this
device_st = 0
device_end = 5

#Path to the dataset, modify this
data_path = "data2/vehicle{}.csv"

#Path to your certificates, modify this
thing_name_formatter =  "vehi{}"
result_subscription_formatter = "result/{}"
certificate_formatter = "./certificates/device_{}.certificate.pem"
key_formatter = "./certificates/device_{}.private.pem"


class MQTTClient:
    def __init__(self, thing_name, cert, key):
        # For certificate based connection
        self.thing_name = str(thing_name)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.thing_name)
        #TODO 2: modify your broker address
        self.client.configureEndpoint("a297wdw6ut18hu-ats.iot.us-east-1.amazonaws.com", 8883)#44.193.72.121#a297wdw6ut18hu-ats.iot.us-east-1.amazonaws.com
        self.client.configureCredentials("./AmazonRootCA1.pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        

    def customOnMessage(self,message):
        #TODO3: fill in the function to show your received message
        print("client {} received payload {} from topic {}".format(self.thing_name, message.payload, message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        #You don't need to write anything here
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        #You don't need to write anything here
        pass


    def publish(self, Payload="payload"):
        #TODO4: fill in this function for your publish
        self.client.subscribeAsync(result_subscription_formatter.format(self.thing_name), 0, ackCallback=self.customSubackCallback)
        self.client.publishAsync("report", Payload, 0, ackCallback=self.customPubackCallback)


print("Loading vehicle data...")
data = []
for i in range(5):
    a = pd.read_csv(data_path.format(i))
    data.append(a)

print("Initializing MQTTClients...")
clients = []
for device_id in range(device_st, device_end):
    client = MQTTClient(thing_name_formatter.format(device_id),certificate_formatter.format(device_id) ,key_formatter.format(device_id))
    client.client.connect()
    clients.append(client)

timestep_time=0
while timestep_time < 258:
    for i, c in enumerate(clients):
        if len(data[i]['vehicle_CO2']) <= timestep_time:
            break
        Payload = json.dumps({
            "thing_name": c.thing_name,
            "CO2": data[i]['vehicle_CO2'][timestep_time]
        })
        print('{} Publish:'.format(timestep_time) + Payload)
        c.publish(Payload)
    timestep_time = timestep_time + 1
    time.sleep(2)
    print('\n')

for c in clients:
    c.client.disconnect()

print("All devices disconnected")

exit()




