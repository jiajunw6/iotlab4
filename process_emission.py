
#
# Copyright 2010-2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#

# greengrassHelloWorld.py
# Demonstrates a simple publish to a topic using Greengrass core sdk
# This lambda function will retrieve underlying platform information and send
# a hello world message along with the platform information to the topic
# 'hello/world'. The function will sleep for five seconds, then repeat.
# Since the function is long-lived it will run forever when deployed to a
# Greengrass core.  The handler will NOT be invoked in our example since
# the we are executing an infinite loop.

import logging
import platform
import sys
import json

from threading import Timer

import greengrasssdk

# Setup logging to stdout
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# Creating a greengrass core sdk client
client = greengrasssdk.client("iot-data")

# Retrieving platform information to send from the Lambda function - Greengrass_HelloWorld_0PElz
my_platform = platform.platform()
data={}

# When deployed to a Greengrass core, this code will be executed immediately
# as a long-lived lambda function.  The code will enter the infinite while
# loop below.
# If you execute a 'test' on the Lambda Console, this test will fail by
# hitting the execution timeout of three seconds.  This is expected as
# this function never returns a result.
def get_input_topic(context):
    try:
        topic = context.client_context.custom['subject']
    except Exception as e:
        logging.error('Topic could not be parsed. ' + repr(e))
    return topic

def echoback(event,context):
    try:
        event['From']='Greengrass Echoback Function'
        client.publish(
            topic="hello/world", queueFullPolicy="AllOrException", payload=json.dumps(event)
        )
       
    except Exception as e:
        logger.error("Failed to publish message: " + repr(e))

    # Asynchronously schedule this function to be run again in 5 seconds



# Start executing the function above
client.publish(
    topic="hello/world", queueFullPolicy="AllOrException", payload="Hello! The lambda has started."
)

# This is a dummy handler and will not be invoked
# Instead the code above will be executed in an infinite loop for our example
def function_handler(event, context):
    global data
    if get_input_topic(context)=='lambda/trigger':
        echoback(event,context)
    if get_input_topic(context)=='report':
        if event['thing_name'] not in data :
            data[event['thing_name']]=[]
        data[event['thing_name']].append(event['CO2'])
        client.publish(
            topic="result/{}".format(event['thing_name']), queueFullPolicy="AllOrException", payload=json.dumps({
                'MaxCO2':max(data[event['thing_name']])
            })
        )

    return

